ARG FROM
FROM $FROM

ENV CACERTS ${JAVA_HOME}/lib/security/cacerts

RUN curl   -o /tmp/cern-root-ca-2.crt "https://cafiles.cern.ch/cafiles/certificates/CERN%20Root%20Certification%20Authority%202.crt" && \
      curl -o /tmp/cern-ca.crt        "https://cafiles.cern.ch/cafiles/certificates/CERN%20Certification%20Authority.crt" && \
      curl -o /tmp/cern-ca-1.crt      "https://cafiles.cern.ch/cafiles/certificates/CERN%20Certification%20Authority(1).crt" && \
      curl -o /tmp/cern-grid-ca-1.crt      "https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority(1).crt" && \
      keytool -noprompt -import -cacerts -storepass changeit -alias CERN_ROOT_CA_2 -file "/tmp/cern-root-ca-2.crt" && \
      keytool -noprompt -import -cacerts -storepass changeit -alias CERN_CA -file "/tmp/cern-ca.crt" && \
      keytool -noprompt -import -cacerts -storepass changeit -alias CERN_CA_1 -file "/tmp/cern-ca-1.crt" && \
      keytool -noprompt -import -cacerts -storepass changeit -alias CERN_GRID_CA_1 -file "/tmp/cern-grid-ca-1.crt" && \
      mkdir -p ~/.ssh && \
      /bin/bash -c 'echo -e "Host gitlab.cern.ch\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config' && \
      rm /tmp/*.crt

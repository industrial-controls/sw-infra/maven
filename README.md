# Maven CERN

An official Maven Docker image enriched with CERN customizations.

# How to use

The entrypoint triggers a mvn call.
Mount your maven root project folder as a volume to /build and start up the docker image.
You can pass mvn arguments directly on the command line :

    docker run -ti --rm -v `pwd`:/build -w /build gitlab-registry.cern.ch/industrial-controls/sw-infra/maven-cern:latest package

You can also mount `/build/target` as a persistent volume if you wish to collect the output of your build.

# How to release

First, start a gitflow release branch and update the version to a non-SNAPSHOT:

    export NEW_VERSION=<new version>
    
    git flow release start $NEW_VERSION
    mvn versions:set -DnewVersion=$NEW_VERSION
    git commit -a -m "Preparing version $NEW_VERSION"
    
Then, refine the release as needed. When you are ready :

    git flow release finish $NEW_VERSION
    git push --tags origin
    
The release will be automatically deployed by Gitlab CI.

Once back on the develop branch, update the version and git push

    mvn versions:set -DnewVersion=<new SNAPSHOT version>
    git commit -a -m "Preparing next SNAPSHOT" && git push
    
# How to build manually (example)

    docker build --build-arg FROM="maven:3.6-jdk-11" \
         -t maven-cern .
         
